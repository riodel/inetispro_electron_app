let HID = require('node-hid');
let usbDetect = require('usb-detection');
const {ipcRenderer} = require('electron');



let hidDevices = HID.devices();
let didFinishLoad = false;
let deviceFound = false;

let ST_CHRGR_ID = {
    AppVersionMajor:'',
    AppVersionMinor:'',
    BootVersionMajor: '',
    BootVersionMinor:'',
    e_TrackerState:'',
    e_TrckrChargingState:'',
    BattPercentage: '',
    TrackerVersionMajor:'',
    TrackerVersionMinor:'',
    TrackerBattVoltage:'',
    s16_TrackerTemp:'',
    st_TrackerSystemState:'',
    e_TrackerPassState:'',
    b_TrackerTimeValid:'',
    b_GpsEphemNeeded:'',
    u32_NumberOfDataLogPos:''
}

const ComChrgrFrameStruct = {
  WindowsReserved:0,
  command: '',
  Data:[]
}



ipcRenderer.on('appLoaded',(e, args)	=>{
	console.log("did-finish-load" + args);
	if(args == "did-finish-load"){
		didFinishLoad = true;
    setTimeout(() => {
      startUp();
    },100)
	}
})

scan = () => {
  hidDevices.forEach((device) => {
    if(device.vendorId == 1240){
      console.log("My Device: " + device);
      deviceFound = true;
      }
  })
}
startUp = () => {
  scan();
  if(deviceFound == false){
    console.log("Tracker Removed");
  }
  else{
    console.log("Tracker is connected - run rxProcess()");
    rxProces();
  }
}




rxProces = () => {
  console.log("rxProcess here");
  let hidDevice = new HID.HID(1240,63);

  hidDevice.on('data', (data) => {
    const bufferData = new Uint16Array(data);

    console.log("Buffer Data: " + bufferData);

    let myResponse = Object.create(ComChrgrFrameStruct);

    console.log("myResponse: " + myResponse);

    bufferData.forEach((bufferItem,index) => {
      if(index == 0){
        myResponse.command = bufferItem
      }
      else{
        myResponse.Data.push(bufferItem);

      }
    })

    // console.log("#!--------------------------------------------");
    // console.log("myResponse.WindowsReserved: " + myResponse.WindowsReserved);
    // console.log("myResponse.command: " + myResponse.command);
    // myResponse.Data.forEach((item) => {
    //   console.log("myResponse.Data[]: " + item);
    // })
    // console.log("--------------------------------------------!#");

    if (myResponse.command != 0){
      if(myResponse.command == 06){}
      else if (myResponse.command == 03) {
        console.log("Read Charger Command - 0x03");
        let st_ChrgrId = Object.create(ST_CHRGR_ID);

        st_ChrgrId.AppVersionMajor = myResponse.Data[0];
        console.log("AppVersionMajor: " + st_ChrgrId.AppVersionMajor);
        st_ChrgrId.AppVersionMinor = myResponse.Data[1];
        console.log("AppVersionMinor: " + st_ChrgrId.AppVersionMinor);

        let i = 8;
        st_ChrgrId.BootVersionMajor = myResponse.Data[i++];
        console.log("BootVersionMajor: " + st_ChrgrId.BootVersionMajor);
        st_ChrgrId.BootVersionMinor = myResponse.Data[i++];
        console.log("BootVersionMinor: " + st_ChrgrId.BootVersionMinor);
        st_ChrgrId.e_TrackerState = myResponse.Data[i++];
        if(st_ChrgrId.e_TrackerState == 0 ){
          st_ChrgrId.e_TrackerState = 'TRACKER_NOT_CONNECTED'
        }
        else if (st_ChrgrId.e_TrackerState == 1) {
          st_ChrgrId.e_TrackerState = 'TRACKER_PRESENT_APP'
        }
        else if (st_ChrgrId.e_TrackerState == 2) {
          st_ChrgrId.e_TrackerState = 'TRACKER_PRESENT_BOOT'
        }
        else if (st_ChrgrId.e_TrackerState == 3) {
          st_ChrgrId.e_TrackerState = 'TRACKER_PRESENT_TEST'
        }
        else if (st_ChrgrId.e_TrackerState == 4) {
          st_ChrgrId.e_TrackerState = 'TRACKER_UNKNOWN'
        }

        console.log("Tracker State: " +st_ChrgrId.e_TrackerState  );

        st_ChrgrId.e_TrckrChargingState = myResponse.Data[i++];
        if(st_ChrgrId.e_TrckrChargingState == 0){
        st_ChrgrId.e_TrckrChargingState = 'CHARGER_STATE_UNKNOWN' //Unknown.
        // console.log(TrackerChargingState);
        }else if(st_ChrgrId.e_TrckrChargingState == 1){
          st_ChrgrId.e_TrckrChargingState = 'CHARGER_STATE_NOT_CONNECTED' //Not connected.
          // console.log(st_ChrgrId.e_TrckrChargingState);
        }else if(st_ChrgrId.e_TrckrChargingState == 2){
          st_ChrgrId.e_TrckrChargingState = 'CHARGER_STATE_CONNECTED' //Connected
          // console.log(st_ChrgrId.e_TrckrChargingState);
        }else if(st_ChrgrId.e_TrckrChargingState == 3){
          st_ChrgrId.e_TrckrChargingState = 'CHARGER_STATE_CHARGE_END' //Charge ended.
          // console.log(st_ChrgrId.e_TrckrChargingState);
        }else if(st_ChrgrId.e_TrckrChargingState == 4){
          st_ChrgrId.e_TrckrChargingState = 'CHARGER_STATE_CHARGING' //Charging
          // console.log(st_ChrgrId.e_TrckrChargingState);
        }else if(st_ChrgrId.e_TrckrChargingState == 5){
          st_ChrgrId.e_TrckrChargingState = 'CHARGER_STATE_FLOATING' //Floating.
          // console.log(st_ChrgrId.e_TrckrChargingState);
        }else if(st_ChrgrId.e_TrckrChargingState == 6){
          st_ChrgrId.e_TrckrChargingState = 'CHARGER_STATE_PAUSED' //Charge paused.
          // console.log(st_ChrgrId.e_TrckrChargingState);
        }

        console.log("Charging  State: " +st_ChrgrId.e_TrckrChargingState  );


          st_ChrgrId.BattPercentage = myResponse.Data[i++];
          console.log("Battery Percentage: " + st_ChrgrId.BattPercentage);

          let temp_Voltage;
          temp_voltage = myResponse.Data[i++];  //[15]
          temp_voltage *= 256;
          temp_voltage += myResponse.Data[i++]; //[16]

          st_ChrgrId.TrackerBattVoltage = temp_voltage / 1000;

          console.log("Tracker Battery Voltage: " + st_ChrgrId.TrackerBattVoltage);

          st_ChrgrId.TrackerVersionMajor = myResponse.Data[i++];
          st_ChrgrId.TrackerVersionMinor = myResponse.Data[i++];

          console.log("Tracker Version: " + st_ChrgrId.TrackerVersionMajor + " " + st_ChrgrId.TrackerVersionMinor);

          let temp_temperature;

          temp_temperature = myResponse.Data[i++];
          st_ChrgrId.s16_TrackerTemp = temp_temperature - 100;
          console.log("Temperature: " + st_ChrgrId.s16_TrackerTemp);

          st_ChrgrId.e_TrackerPassState = myResponse.Data[i++];
          if(st_ChrgrId.e_TrackerPassState == 0){
            st_ChrgrId.e_TrackerPassState = 'TRACKER_PASSWORD_RESP_BAD_PASSWORD'
          console.log("Tracker Pass State: "+ st_ChrgrId.e_TrackerPassState);

          }else if(st_ChrgrId.e_TrackerPassState == 1){
            st_ChrgrId.e_TrackerPassState = 'TRACKER_PASSWORD_RESP_GOOD_PASSWORD'
          console.log("Tracker Pass State: "+ st_ChrgrId.e_TrackerPassState);

          }else if(st_ChrgrId.e_TrackerPassState == 2){
            st_ChrgrId.e_TrackerPassState = 'TRACKER_PASSWORD_LOCKED_OR_PUK'
          console.log("Tracker Pass State: "+ st_ChrgrId.e_TrackerPassState);

          }else if(st_ChrgrId.e_TrackerPassState == 3){
            st_ChrgrId.e_TrackerPassState = 'TRACKER_PASSWORD_PUK_LOCKED'
          console.log("Tracker Pass State: "+ st_ChrgrId.e_TrackerPassState);

          }


          st_ChrgrId.b_TrackerTimeValid = myResponse.Data[i++];
            st_ChrgrId.b_GpsEphemNeeded = myResponse.Data[i++];



            st_ChrgrId.u32_NumberOfDataLogPos = myResponse.Data[i++]; //[23] correct index position for the tracker position in the memory

            st_ChrgrId.u32_NumberOfDataLogPos |= myResponse.Data[i++] << 8; // [24] correct index position for the tracker position in the memory : 8 << shifts 8 zero position to the right

            st_ChrgrId.u32_NumberOfDataLogPos |= myResponse.Data[i++] << 16; //[25]

            st_ChrgrId.u32_NumberOfDataLogPos |= myResponse.Data[i++] << 24 // [26]

              console.log("Positions in Memory: " +st_ChrgrId.u32_NumberOfDataLogPos  );


              setTimeout(() =>{
                console.log("ST_CHRGR_ID.AppVersionMajor: " + st_ChrgrId.AppVersionMajor);
              },5000)


      }
      else if (myResponse.command == 07) {
        console.log("TRACKER_CMD_SEND_GEN_CMD - 0x07")

      }
    }

  })


}
