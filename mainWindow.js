//Modules
const {BrowserWindow,ipcMain} = require('electron')

//BrowserWindow Instance
exports.BrowserWindow


exports.createWindow = () =>{

    this.win = new BrowserWindow({
        width: 800,
        heght:500,
        minWidth:700,
        maxWidth:1200,
        minHeight:500
    })



    this.win.loadURL(`file://${__dirname}/renderer/main.html`)

    this.win.webContents.on('did-finish-load',()=>{
  this.win.webContents.send('appLoaded', 'did-finish-load');
})


    this.win.on('closed', () => {
        this.win = null
    })
}
